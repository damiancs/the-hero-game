# coding=utf-8
"""
Test module for Character class
"""
import random
from unittest import TestCase
from unittest.mock import MagicMock

from testfixtures import LogCapture

from character import Character
from skills import RapidStrike, MagicShield


class TestCharacter(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.hero_dict = {
            "name": "Hero",
            "health": random.randint(90, 100),
            "strength": random.randint(50, 60),
            "defence": random.randint(30, 40),
            "speed": random.randint(25, 50),
            "luck": random.randint(25, 50),
            "skills": set()
        }
        cls.beast_dict = {
            "name": "Beast",
            "health": random.randint(40, 50),
            "strength": random.randint(40, 50),
            "defence": random.randint(30, 40),
            "speed": random.randint(10, 20),
            "luck": random.randint(10, 20),
            "skills": set()
        }

    def test_attributes(self):
        hero = Character(**self.hero_dict)
        beast = Character(**self.beast_dict)
        self.assertDictEqual(
            dict(hero.__dict__),
            {f"_Character__{key}": self.hero_dict[key] for key in self.hero_dict}
        )
        self.assertDictEqual(
            dict(beast.__dict__),
            {f"_Character__{key}": self.beast_dict[key] for key in self.beast_dict}
        )

        def __assertions(char, char_dict):
            self.assertEqual(char.name, char_dict["name"])
            self.assertEqual(char.health, char_dict["health"])
            self.assertEqual(char.strength, char_dict["strength"])
            self.assertEqual(char.defence, char_dict["defence"])
            self.assertEqual(char.speed, char_dict["speed"])
            self.assertEqual(char.luck, char_dict["luck"])
            self.assertEqual(char.skills, char_dict["skills"])

        __assertions(hero, self.hero_dict)
        __assertions(beast, self.beast_dict)

    def test_attack_and_defend(self):
        hero = Character(**self.hero_dict)
        beast = Character(**self.beast_dict)
        hero.attack(beast)
        beast.attack(hero)
        hero_health = self.hero_dict["health"] - (self.beast_dict["strength"] - self.hero_dict["defence"])
        beast_health = self.beast_dict["health"] - (self.hero_dict["strength"] - self.beast_dict["defence"])
        self.assertEqual(beast.health, beast_health if beast_health >= 0 else 0)
        self.assertEqual(hero.health, hero_health if hero_health >= 0 else 0)

        hero = Character("Hero", 20, 50, 10)
        beast = Character("Beast", 20, 50, 10)

        hero.attack(beast)
        beast.attack(hero)
        self.assertEqual(hero.health, 0)
        self.assertEqual(beast.health, 0)

        hero.defend = MagicMock(return_value=hero.defence)
        beast.defend = MagicMock(return_value=beast.defence)

        hero.attack(beast)
        beast.attack(hero)

        hero: MagicMock
        beast: MagicMock
        hero.defend.assert_called_with(beast)
        beast.defend.assert_called_with(hero)

    def test_magic_methods(self):
        char = Character("Char", 20, 50, 10)
        damage = random.randint(5, 10)
        final_health = char.health - damage
        char - damage
        self.assertEqual(char.health, final_health)

        char = Character()
        self.assertEqual(char.name, "Unnamed")
        self.assertEqual(char.health, 100)
        self.assertEqual(char.strength, 80)
        self.assertEqual(char.defence, 55)
        self.assertEqual(char.speed, 50)
        self.assertEqual(char.luck, 30)
        self.assertEqual(char.skills, set())

        self.assertEqual(
            repr(char),
            "-------------------------\n"
            "|  Unnamed              |\n"
            "|                       |\n"
            "|    - Health:   100    |\n"
            "|    - Strength:  80    |\n"
            "|    - Defence:   55    |\n"
            "|    - Speed:     50    |\n"
            "|    - Luck:      30    |\n"
            "-------------------------"
        )

    def test_with_skills(self):
        magic_shield = MagicShield(100)
        rapid_strike = RapidStrike(100)
        magic_shield.use = MagicMock(return_value=12)
        rapid_strike.use = MagicMock(return_value=0)
        hero = Character(skills={rapid_strike})
        beast = Character(skills={magic_shield})

        hero.attack(beast)
        rapid_strike.use.assert_called_with(hero, beast)
        magic_shield.use.assert_called_with(hero, beast)

    def test_logging(self):
        hero = Character(name="Hero", strength=100, defence=20)
        beast = Character(name="Beast", strength=40, defence=50)
        with LogCapture() as capture:
            hero.attack(beast)
            beast.attack(hero)
            capture.check(
                ('The Hero Game', 'INFO', '| * Hero attacks Beast (normal damage: 50)\n|'),
                ('The Hero Game', 'INFO', '| * Beast attacks Hero (normal damage: 20)\n|')
            )
