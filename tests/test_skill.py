# coding=utf-8
"""
Test module for Skill, MagicShield and RapidStrike classes
"""
import random
from unittest import TestCase
from unittest.mock import patch

from testfixtures import LogCapture

from exceptions import PercentageError
from skills import MagicShield, RapidStrike
from skills.skill import Skill


class TestSkills(TestCase):

    @patch("character.Character")
    def test_exceptions(self, character):
        """
        Testing exceptions raised in class Skill
        :param character: Mock for character.Character
         :type character: character.Character
        """
        with self.assertRaises(PercentageError):
            Skill(-1, True)
        with self.assertRaises(PercentageError):
            Skill(101, True)
        with self.assertRaises(NotImplementedError):
            Skill(100, True).use(character, character)

    def test_success(self):
        chance, offensive = random.randint(0, 100), random.randint(1, 100) % 2 == 0
        skill = Skill(chance, offensive)
        self.assertEqual(skill.chance, chance)
        self.assertEqual(skill.offensive, offensive)

    @patch("character.Character")
    def test_magic_shield(self, character):
        chance = random.randint(0, 100)
        skill = MagicShield(chance)
        self.assertEqual(skill.chance, chance)
        self.assertFalse(skill.offensive)

        strength = random.randint(50, 100)
        defence = random.randint(20, 50)
        result = (strength - defence) // 2

        character.name = "Char"
        character.strength = strength
        character.defence = defence
        with LogCapture() as capture:
            skill_use_result = skill.use(character, character)
            capture.check(('The Hero Game', 'INFO', '|    + Char uses MagicShield against Char\n|'))
        self.assertEqual(skill_use_result, result)

    @patch("character.Character")
    def test_rapid_strike(self, character):
        chance = random.randint(0, 100)
        skill = RapidStrike(chance)
        self.assertEqual(skill.chance, chance)
        self.assertTrue(skill.offensive)

        character.name = "Char"
        with LogCapture() as capture:
            skill_use_result = skill.use(character, character)
            capture.check(('The Hero Game', 'INFO', '|    + Char uses RapidStrike against Char\n|'))
        self.assertEqual(skill_use_result, 0)
