# coding=utf-8
"""
Module containing the basic implementation of the Skill class that should be the base for every skill
"""
from exceptions import PercentageError
from logger import LOGGER


class Skill:
    """
    Base class to inherit from when building skills
    """

    def __init__(self, chance: int, offensive: bool) -> None:
        """
        :param chance: The change that the attacker has to use this skill (percentage)
         :type chance: int
        :param offensive: Boolean flag that tells if the skill should be used when attacking or when defending
         :type offensive: bool
        :raise PercentageError: If the change is not an integer inside the interval [0, 100]
        """
        self.__chance = chance
        self.__offensive = offensive

        self.__raise_for_errors()

    @property
    def chance(self):
        """Getter for `change` attribute"""
        return self.__chance

    @property
    def offensive(self):
        """Getter for `offensive` attribute"""
        return self.__offensive

    def use(self, attacker: "Character", defender: "Character"):
        """
        The usage of the skill needs to be implemented in the child class

        :param attacker: The attacking character
         :type attacker: Character
        :param defender: The defending character
         :type defender: Character
        :raise: NotImplementedError to force the implementation in the child class
        """
        raise NotImplementedError()

    def __raise_for_errors(self):
        """
        Checks if there are any errors in values of the attributes

        :raise: PercentageError if change is not an integer inside the interval [0, 100]
        """
        if self.__chance < 0 or self.__chance > 100:
            raise PercentageError("Skill chance should be a percentage (integer value in interval [0, 100])")


def log_skill_use(func):
    """Logs the usage of one skill so it is meant to be used in the child classes"""

    def wrapper(skill: Skill, char_one: "Character", char_two: "Character"):
        """Wrapper method that does the actual logging and returns the value from the decorated method call"""
        LOGGER.info(
            "|    + %s uses %s against %s\n|",
            char_one.name if skill.offensive else char_two.name,
            skill.__class__.__name__,
            char_two.name if skill.offensive else char_one.name
        )
        return func(skill, char_one, char_two)

    return wrapper
