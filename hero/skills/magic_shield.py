# coding=utf-8
"""
This modules contains the implementation of the MagicShield skill that gives the possessor the ability to halve the
damage taken during an attack
"""
from skills.skill import Skill, log_skill_use


class MagicShield(Skill):
    """Gives the character the capability to halve the damage taken"""

    def __init__(self, chance) -> None:
        """
        :param chance: The change that the attacker has to use this skill (percentage)
         :type chance: int
        """
        super(MagicShield, self).__init__(chance=chance, offensive=False)

    @log_skill_use
    def use(self, attacker: "Character", defender: "Character") -> int:
        """
        The defending character is given the power to halve the damage taken

        :param attacker: The attacking character
         :type attacker: Character
        :param defender: The defending character
         :type defender: Character
        :return: Half of the usual damage
         :rtype: int
        """
        return (attacker.strength - defender.defence) // 2
