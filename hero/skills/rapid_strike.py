# coding=utf-8
"""
This modules contains the implementation of the RapidStrike skill that gives the possessor the ability to attack twice
"""
from skills.skill import Skill, log_skill_use


class RapidStrike(Skill):
    """Gives the character the capability to attack again"""

    def __init__(self, chance: int) -> None:
        """
        :param chance: The change that the attacker has to use this skill (percentage)
         :type chance: int
        """
        super(RapidStrike, self).__init__(chance=chance, offensive=True)

    @log_skill_use
    def use(self, attacker: "Character", defender: "Character") -> int:
        """
        The character is given the power to attack again
        Since we call attack again we need to restrict the usage of the same power so we skip it on the next call

        :param attacker: The attacking character
         :type attacker: Character
        :param defender: The defending character
         :type defender: Character
        :return: Zero since the skill doesn't actually make any damage itself
         :rtype: int
        """
        attacker.attack(defender, [self.__class__.__name__])
        return 0
