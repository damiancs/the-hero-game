# coding=utf-8
"""
Module that expose all the defined skills
"""
from skills.magic_shield import MagicShield
from skills.rapid_strike import RapidStrike

__all__ = ["MagicShield", "RapidStrike"]
