# coding=utf-8
"""
Module containing the implementation of the Character class used for heroes and beasts
"""
import random

from logger import LOGGER


class Character:
    """
    Model class for one character in the game (hero or beast)
    """

    def __init__(self, name: str = None, health: int = None, strength: int = None, defence: int = None,
                 speed: int = None, luck: int = None, skills: "Set[Skill]" = None) -> None:
        """
        :param name: The given name of the character
         :type name: str
        :param health: Starting health of the character
         :type health: int
        :param strength: Attacking power of the character
         :type strength: int
        :param defence: Defending power of the character
         :type defence: int
        :param speed: Speed of the character
         :type speed: int
        :param luck: Luck percentage of the character
         :type luck: int
        :param skills: A set of offensive and defensive skills of the character
         :type skills: set
        """
        self.__name = name or "Unnamed"
        self.__health = health or 100
        self.__strength = strength if strength is not None else 80
        self.__defence = defence if defence is not None else 55
        self.__speed = speed if speed is not None else 50
        self.__luck = luck if luck is not None else 30
        self.__skills = skills or set()

    @property
    def name(self) -> str:
        """Getter for `name` attribute"""
        return self.__name

    @property
    def health(self) -> int:
        """Getter for `health` attribute"""
        return self.__health

    @property
    def strength(self) -> int:
        """Getter for `strength` attribute"""
        return self.__strength

    @property
    def defence(self) -> int:
        """Getter for `defence` attribute"""
        return self.__defence

    @property
    def speed(self) -> int:
        """Getter for `speed` attribute"""
        return self.__speed

    @property
    def luck(self) -> int:
        """Getter for `luck` attribute"""
        return self.__luck

    @property
    def skills(self) -> "Set[Skill]":
        """Getter for `skills` attribute"""
        return set(skill.__class__.__name__ for skill in self.__skills)

    def attack(self, other: "Character", skip_skills: "Set[str]" = None) -> None:
        """
        Calculates the damage done by using the offensive skills if any available
        The actual value is the maximum of `0` and `strength - defense` so we don't add to the health by mistake

        :param other: The attacked character
         :type other: Character
        :param skip_skills: The skills that cannot be used during the attack
         :type skip_skills: Set[str]
        """
        LOGGER.info("| * %s attacks %s (normal damage: %d)\n|", self.name, other.name, self.strength - other.defence)
        defense = other.defend(self)
        strength = self.__strength
        for skill in self.__skills:
            if (skip_skills and skill.__class__.__name__ in skip_skills) or \
                    not skill.offensive or skill.chance < random.randint(0, 100):
                continue
            strength += skill.use(self, other)
        other -= max(0, strength - defense)

    def defend(self, other: "Character", skip_skills: "Set[str]" = None) -> int:
        """
        Calculates the defense by using the defensive skills if any available

        :param other: The attacking character
         :type other: Character
        :param skip_skills: The skills that cannot be used during the defense
         :type skip_skills: Set[str]
        :return: The defense that can be exercised
         :rtype: int
        """
        defence = self.__defence
        for skill in self.__skills:
            if (skip_skills and skill.__class__.__name__ in skip_skills) or skill.offensive or \
                    skill.chance < random.randint(0, 100):
                continue
            defence += skill.use(other, self)
        return defence

    def __sub__(self, damage: int) -> "Character":
        """
        Lowers the health with the given damage

        :param damage: The damage caused during an attack (min of health and damage so the health won't go bellow 0)
         :type damage: int
        """
        self.__health -= min(damage, self.__health)
        return self

    def __repr__(self) -> str:
        """Returns a string representation of the character"""
        return f"-------------------------" \
               f"\n|  {self.__name:21}" \
               f"|\n|                       " \
               f"|\n|    - Health:   {self.__health:3d}    " \
               f"|\n|    - Strength: {self.__strength:3d}    " \
               f"|\n|    - Defence:  {self.__defence:3d}    " \
               f"|\n|    - Speed:    {self.__speed:3d}    " \
               f"|\n|    - Luck:     {self.__luck:3d}    " \
               f"|\n-------------------------"
