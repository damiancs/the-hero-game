# coding=utf-8
"""
The entry point for the game
"""

from game import Game

__all__ = ["Game"]

if __name__ == "__main__":
    GAME = Game()
    PLAY = True
    while PLAY:
        GAME.play()
        PLAY = input("|\n| Wanna replay? (yY/...) -> ").lower().startswith("y")
