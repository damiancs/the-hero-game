# coding=utf-8
"""
Module that contains the gameplay logic for the Hero game
"""
import os
import random

from character import Character
from logger import LOGGER
from skills import MagicShield, RapidStrike

HEROES_NO = int(os.environ.get("HEROES_NO", "1"))
BEASTS_NO = int(os.environ.get("BEASTS_NO", "1"))

HERO_NAMES = os.environ.get("HERO_NAMES", "Orderus").split(",")
BEAST_NAMES = os.environ.get("BEAST_NAMES", "Belzebuth").split(",")


class Game:
    """
    Class that groups the logic for the gameplay
    """

    def __init__(self, heroes: int = HEROES_NO, beasts: int = BEASTS_NO) -> None:
        """
        :param heroes: Number of heroes to be spawned
         :type heroes: int
        :param beasts: Number of beasts to be spawned
         :type beasts: int
        """
        self.__heroes = [
            Character(
                name=random.choice(HERO_NAMES),
                health=random.randint(70, 100),
                strength=random.randint(70, 80),
                defence=random.randint(45, 55),
                speed=random.randint(40, 50),
                luck=random.randint(10, 30),
                skills={RapidStrike(chance=10), MagicShield(chance=20)}
            ) for _ in range(heroes)
        ]
        self.__beasts = [
            Character(
                name=random.choice(BEAST_NAMES),
                health=random.randint(60, 90),
                strength=random.randint(60, 90),
                defence=random.randint(40, 60),
                speed=random.randint(40, 60),
                luck=random.randint(25, 40)
            ) for _ in range(beasts)
        ]
        self.stop = True

    def __chose_attackers(self) -> "Tuple[List[Character], List[Character]]":
        """
        Chooses which characters attack first (heroes or beasts) according to the game rules:
            a) the ones with more speed
            b) for equal speed the ones with more luck
            c) for equal speed and luck the heroes attack first (this was added by myself)

        :return: A tuple with attackers and defenders (in this specific order)
         :rtype: Tuple[List[Character], List[Character]]
        """
        speed_one = sum([char.speed for char in self.__heroes])
        speed_two = sum([char.speed for char in self.__beasts])
        luck_one = sum([char.luck for char in self.__heroes])
        luck_two = sum([char.luck for char in self.__beasts])

        if speed_one > speed_two or (speed_one == speed_two and luck_one >= luck_two):
            return self.__heroes, self.__beasts
        return self.__beasts, self.__heroes

    def __check_game_over(self) -> None:
        """Sets the stop flag if the game is over (one of the teams runed out of health)"""
        self.stop = not sum([char.health for char in self.__heroes]) or not sum([char.health for char in self.__beasts])

    def __stats(self, remaining: "List[Character]" = None) -> None:
        """
        Logs the stats in the game at some point

        :param remaining: Used for passing to the method the remaining characters (to be displayed as winners)
         :type remaining: List[Character]
        """
        LOGGER.info("--------------------------------------------------------------------------------")
        LOGGER.info("\n=====    HEROES     =====\n")
        for hero in self.__heroes:
            LOGGER.info(hero)

        LOGGER.info("\n=====    BEASTS     =====\n")
        for beast in self.__beasts:
            LOGGER.info(beast)
        LOGGER.info("--------------------------------------------------------------------------------\n")

        if self.stop:
            LOGGER.info("--------------------------------------------------------------------------------")
            LOGGER.info("|\n|                                   GAME OVER\n|")
            LOGGER.info("|                              WINNER(s): %s",
                        ", ".join(character.name for character in remaining or []))
            LOGGER.info("|\n--------------------------------------------------------------------------------")

    def play(self) -> None:
        """Plays the game from the beginning (the heroes and the beasts are not changed)"""
        self.stop = False

        attackers, defenders = self.__chose_attackers()
        self.__stats()

        LOGGER.info("================================================================================")
        LOGGER.info("|\n| To start the game: %15s\n|", attackers[0].name)

        game_round = 1
        while not self.stop:
            random.shuffle(attackers)
            random.shuffle(defenders)

            LOGGER.info("================================================================================")
            LOGGER.info("|\n| Starting round number %4d\n|", game_round)
            input("|\n| Press <ENTER> to move on...\n|")

            idx = 0
            while not self.stop and idx < len(attackers):
                attackers[idx].attack(random.choice(defenders))
                self.__check_game_over()
                idx += 1

            idx = 0
            while not self.stop and idx < len(defenders):
                defenders[idx].attack(random.choice(attackers))
                self.__check_game_over()
                idx += 1

            attackers = list(filter(lambda char: char.health > 0, attackers))
            defenders = list(filter(lambda char: char.health > 0, defenders))
            self.__stats(attackers or defenders)
            game_round += 1
