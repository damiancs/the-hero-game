# coding=utf-8
"""
This module contains the Exceptions used in this application
"""


class PercentageError(Exception):
    """
    This error class is raise when a percentage is set outside of the [0, 100] interval
    """
