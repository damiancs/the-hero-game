# coding=utf-8
"""
Module containing the default logger for the application
"""
import logging
import sys

LOGGER = logging.getLogger("The Hero Game")
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(logging.StreamHandler(sys.stdout))
