FROM python:3.8-alpine3.10

RUN mkdir /app
ADD hero /app
WORKDIR /app

CMD ["python", "__main__.py"]

